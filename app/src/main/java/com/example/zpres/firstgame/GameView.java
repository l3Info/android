package com.example.zpres.firstgame;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import static android.app.Activity.RESULT_OK;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private GameLoopThread gameLoopThread;
    private Balle[] balle;
    private int nbBalle = 6;
    private int largeur, hauteur;
    private boolean debut = false;
    private Player joueur;
    private Context context;
    private Intent intent;
    private int accX, accY;
    int x;

    private SensorManager mManager = null;
    private Sensor mAccelerometre = null;

    SensorEventListener mSensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent pEvent) {
            x = (int) pEvent.values[0];
            joueur.deplacer(2 * x * (-1));
        }

        @Override
        public void onAccuracyChanged(Sensor pSensor, int pAccuracy) {

        }
    };

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
                Toast.makeText(context,"Votre Score : " + msg.arg1, Toast.LENGTH_LONG).show();
        }
    };

    public GameView(Context context, Intent intent){
        super(context);
        this.intent = intent;
        this.context = context;
        getHolder().addCallback(this);
        balle = new Balle[nbBalle];
        gameLoopThread = new GameLoopThread(this);

        joueur = new Player(this.getContext(), 0, 0);
        for(int i = 0; i <nbBalle; i++)
            balle[i] = new Balle(this.getContext(), i + 1);

        accX = 0;
        accY = 0;
        mManager = (SensorManager) ( (Activity)context).getBaseContext().getSystemService(Service.SENSOR_SERVICE);
        mAccelerometre = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mManager.registerListener(mSensorEventListener, mAccelerometre, SensorManager.SENSOR_DELAY_GAME);
    }

    public void changementAcc(int x, int y){

    }

    public void doDraw(Canvas canvas){
        if(canvas == null) {return ;}
        canvas.drawColor(Color.WHITE);

        for(int i =0; i < nbBalle; i++)
            balle[i].draw(canvas);
        joueur.draw(canvas);
    }

    public void update() {
        if(debut){
            int deplacement;
            boolean test = false;
            int debutX, debutY, finY;


            debutY = joueur.getY();
            deplacement = joueur.actualiser();
            finY = joueur.getY();
            debutX = joueur.getX();

            for(int i =0; i<nbBalle;i++){
                if(moveOrChoc(balle[i],debutX,debutY,finY, deplacement)){
                    test = true;
                }
                if(balle[i].getY() > hauteur){

                    balle[i].actu(getMinHauteur());
                }

            }
            if(test)
                joueur.acc();
            if(!joueur.etat()){
                debut = false;
                Message msg = handler.obtainMessage();
                msg.arg1 = joueur.getScore();
                //handler.sendMessage(msg);
                quitter();
            }
        }
    }

    public int getMinHauteur(){
        int min = hauteur;
        for(int i =0; i < nbBalle; i++)
            if(balle[i].getY() < min)
                min = balle[i].getY();
        return min;
    }
    public int Max(int a, int b){
        if(a > b)
            return a;
        return b;
    }

    public int Min(int a, int b){
        if(a < b)
            return a;
        return b;
    }
    public boolean moveOrChoc(Balle balle, int debutX, int debutY, int finY, int deplacement){
        int largeurEcran = balle.getLargeurEcran();
        if( ((balle.getX() >= debutX) && (balle.getX() <= debutX + (0.1)*largeurEcran)) || ( (balle.getX() <= debutX) && (balle.getX()+(0.1)*largeurEcran >= debutX)) ){
            int balleDebutY = balle.getY();
            int balleFinY = balle.getY() + deplacement;

            if(deplacement == 0){
                if(debutY > finY){
                    if((balleDebutY >= finY && balleDebutY <= debutY) || (balleDebutY + (0.1)*largeurEcran >= finY && balleDebutY + (0.1)*largeurEcran <= debutY)){
                        balle.deplacer(10000);
                        return true;
                    }
                }
                if(debutY < finY){
                    if((balleDebutY >= debutY && balleDebutY <= finY) || (balleDebutY + (0.1)*largeurEcran >= debutY && balleDebutY + (0.1)*largeurEcran <= finY)){
                        balle.deplacer(10000);
                        return true;
                    }
                }
            } else {
                if(debutY > finY){
                    if(((balleDebutY >= finY) && (balleDebutY <= debutY)) || ( (balleFinY >= finY) && (balleFinY <= debutY))){
                        balle.deplacer(10000);
                        return true;
                    }
                }else {
                    if( ((balleDebutY <= debutY) && (balleFinY + (0.1)*largeurEcran >= debutY) )){
                        balle.deplacer(10000);
                        return true;
                    }
                }
            }

            if( (debutY <= Max(balleDebutY,balleFinY)) && (debutY >= Min(balleDebutY,balleFinY)) || (finY <= Max(balleDebutY,balleFinY)) && (finY >= Min(balleDebutY,balleFinY))) {
                balle.deplacer(10000);
                return true;


            }
        }
        balle.deplacer(deplacement);
        return false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if(gameLoopThread.getState()==Thread.State.TERMINATED) {
            gameLoopThread=new GameLoopThread(this);
        }
        gameLoopThread.setRunning(true);
        gameLoopThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        gameLoopThread.setRunning(false);
        while (retry) {
            try {
                gameLoopThread.join();
                retry = false;
            }
            catch (InterruptedException e) {}
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(!debut) {
            joueur.acc();
            debut = true;
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int w, int h) {
        for(int j=0; j < nbBalle; j++)
            balle[j].resize(w,h);
        joueur.resize(w,h);
        largeur = w;
        hauteur = h;
    }

    public void quitter(){
        intent.putExtra("data", joueur.getScore());
        ((Activity)context).setResult(RESULT_OK, intent);
        ((Activity)context).finish();
    }

}
