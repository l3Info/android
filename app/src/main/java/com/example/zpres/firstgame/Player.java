package com.example.zpres.firstgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


public class Player {
    private BitmapDrawable img = null;
    private int maxVitesse = 100;
    private int vitesseAcc;
    private int x, y;
    private int vitesse;
    private int hauteur, largeur;
    private int largeurEcran, hauteurEcran;
    private Context mContext;
    private boolean etat;
    private int score;


    Player(final Context c,int hauteur, int largeur){
        x = 0;
        y = (int) (hauteur - 0.1 * largeur);
        vitesse = 0;
        mContext = c;
        etat = true;
    }

    int getScore(){
        return score;
    }
    void acc(){
        vitesse += vitesseAcc;
        if(vitesse < vitesseAcc){
            vitesse = vitesseAcc;
        }
        if(vitesse > maxVitesse)
            vitesse = maxVitesse;
    }

    int actualiser(){
        if(vitesse > 0)
            score += vitesse;
        if(y-vitesse < hauteurEcran/2){
            int dif = 0;
            dif = (hauteurEcran/2)-(y-vitesse);
            y = hauteurEcran/2;
            vitesse -= 1;
            return dif;
        }else if(y-vitesse < hauteurEcran - (0.1)*largeurEcran){
            y-= vitesse;
            vitesse -= 1;
            return 0;
        }
        else{
            y = (int) (hauteurEcran - (0.1) * largeurEcran);
            etat = false;
            vitesse-=1;
            return -1;
        }
    }

    int deplacer(int depl){
        if(x+depl >= 0 && x+depl <= (0.9)*largeurEcran )
        x += depl;
        return 0;
    }

    boolean etat(){
        if(etat)
            return etat;
        else{
            y = (int) (hauteurEcran - 0.1 * largeurEcran);
            vitesse = 0;
        }
        return false;
    }

    public BitmapDrawable setImage(final Context c, final int ressource, final int w, final int h)
    {
        Drawable dr = c.getResources().getDrawable(ressource);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public void resize(int wScreen, int hScreen) {
        largeurEcran = wScreen;
        hauteurEcran = hScreen;

        hauteur = wScreen / 10;
        largeur = wScreen / 10;
        y = (int) (hauteurEcran - 0.1 * largeurEcran);
        x = (int) ((largeurEcran/2) - 0.05*largeurEcran);
        img = setImage(mContext, R.mipmap.ninja,largeur,hauteur);
        vitesseAcc = hauteurEcran/45;
        maxVitesse = 3 * (hauteurEcran/45);
    }

    public void draw(Canvas canvas)
    {
        if(img==null) {return;}
        canvas.drawBitmap(img.getBitmap(), x, y, null);
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public int getVitesse(){
        return vitesse;
    }
}
