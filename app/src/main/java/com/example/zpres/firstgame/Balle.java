package com.example.zpres.firstgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.util.Random;

public class Balle {
    private BitmapDrawable img = null;
    private int x,y;
    private int hauteur, largeur;
    private int largeurEcran, hauteurEcran;
    private int zone;

    private final Context mContext;

    public Balle(final Context c, int zon){
        x = (int)(Math.random() * (((int) ((0.9)*largeur-0)) + 1));
        zone = zon;
        mContext = c;
    }
    public Balle(final Context c, int zone, int hauteur){
        x = (int)(Math.random() * (((int) ((0.9)*largeur-0)) + 1));
        this.zone = zone;
        mContext = c;
        this.hauteur = hauteur;
    }

    public BitmapDrawable setImage(final Context c, final int ressource, final int w, final int h)
    {
        Drawable dr = c.getResources().getDrawable(ressource);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    public void resize(int wScreen, int hScreen) {
        largeurEcran = wScreen;
        hauteurEcran = hScreen;

        x = (int)(Math.random() * (((int) ((0.9)*largeurEcran-0)) + 1));
        if(zone > 0 && zone < 7){
                Random rand = new Random();
                int min = (int)(hauteurEcran - (zone+1) * (0.2) *hauteurEcran);
                int max = (int) (hauteurEcran - (zone) * (0.2) * hauteurEcran);
                y = rand.nextInt(max - min + 1) + min;
        } else{
            Random rand = new Random();
            int max = (int) (hauteur - (0.2) * hauteurEcran);
            int min = (int) (hauteur - (0.4) * hauteurEcran);
            y = rand.nextInt(max - min +1) +min;
        }

        hauteur = wScreen / 10;
        largeur = wScreen / 10;
        img = setImage(mContext, R.mipmap.ball,largeur,hauteur);
    }

    public void draw(Canvas canvas)
    {
        if(img==null) {return;}
        canvas.drawBitmap(img.getBitmap(), x, y, null);
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public boolean deplacer(int depl){
        y += depl;
        return false;
    }

    public int getLargeurEcran(){
        return largeurEcran;
    }

    public int getZone(){
        return zone;
    }

    public void reductionZone(){
        zone -=1;
    }

    public void actu(int hauteur){
        Random rand = new Random();
        int max = (int) (hauteur - (0.2) * hauteurEcran);
        int min = (int) (hauteur - (0.4) * hauteurEcran);
        y = rand.nextInt(max - min +1) +min;
        x = (int)(Math.random() * (((int) ((0.9)*largeurEcran-0)) + 1));
    }
}
