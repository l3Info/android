package com.example.zpres.firstgame;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;


public class Jeu extends Activity {
    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    gameView = new GameView(this, getIntent());
    setContentView(gameView);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

}}
