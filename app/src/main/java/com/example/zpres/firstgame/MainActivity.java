package com.example.zpres.firstgame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    private GameView gameView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

        public void test(View v) {
            Intent intent = new Intent(MainActivity.this, Jeu.class);
            startActivityForResult(intent, 2);
        }
    public void afficheClassement(View v){
        Intent intent = new Intent(MainActivity.this, Classement.class);
        intent.putExtra("data", Read());
        startActivity(intent);
    }

    protected void onActivityResult(int codeActivity, int codeRetour, Intent intent)
    {
        if(codeActivity == 2){
            int score = intent.getIntExtra("data",0);
            classement(score);
        }
        if(codeActivity == 3) {
            int score = intent.getIntExtra("score",0);
            int classement = intent.getIntExtra("classement",0);
            String pseudo = intent.getStringExtra("data");
            modifClassement(score,classement, pseudo);
        }
    }

    public void Write(String data){
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        try{
            fOut = openFileOutput("settings.dat",MODE_PRIVATE);
            osw = new OutputStreamWriter(fOut);
            osw.write(data);
            osw.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Settings not saved",Toast.LENGTH_SHORT).show();
        }
        finally {
            try {
                osw.close();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String Read(){
        String data = null;
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(openFileInput("settings.dat")));
            data = in.readLine();
            in.close();
            return data;
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }

    public void classement(int score){
        int place = 10000;

        String reception = "";
        reception = Read();
        String[] donnees = reception.split(";");
        if(donnees.length != 20){
            String data = "";
            for(int i=0;i<9;i++){
                data += 0 + ";" + 0 + ";";
            }
            data += "0;0";
            Write(data);
            reception = Read();
            donnees = reception.split(";");
        }
        for(int i = 0;i<10;i++){
            if(Integer.parseInt(donnees[i*2]) < score || donnees[i*2].equals("")) {
                if(place>i+1) {
                    place = i + 1;
                    Intent intent = new Intent(MainActivity.this, BestScore.class);
                    intent.putExtra("score", score);
                    intent.putExtra("classement", place);
                    startActivityForResult(intent, 3);
                }
            }
        }
    }

    public void modifClassement(int score, int classement, String Pseudo){
        String reception = Read();
        String[] donnees = reception.split(";");
        String data ="";
        String tmpPseudo ="";
        int tmpScore = 0;
        boolean test = true;
        Toast.makeText(MainActivity.this, ""+donnees.length+" "+classement, Toast.LENGTH_LONG).show();
        for(int i = classement-1;(i<10 && test);i++){
            if(donnees[2*i].equals("0"))
            {
                    donnees[(2*i)+1] = Pseudo;
                    donnees[2*i] = "" + score;
                test = false;
            }else {
                tmpPseudo = donnees[(2*i)+1];
                tmpScore = Integer.parseInt(donnees[2*i]);
                donnees[(2*i)+1] = Pseudo;
                donnees[2*i] = "" + score;
                Pseudo = tmpPseudo;
                score = tmpScore;
            }
        }
        for(int i = 0; i<10;i++){
            data += donnees[i*2] + ";" + donnees[(i*2)+1] + ";";
        }
        Write(data);
    }
}