package com.example.zpres.firstgame;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class BestScore extends Activity {
    private int score;
    private EditText ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.best_score);
        getclassement();
        ed = (EditText) findViewById(R.id.editText2);
    }

    public void getclassement(){
        TextView tv = (TextView) findViewById(R.id.textView3);
        String text = "Vous avez le ";
        int classement = getIntent().getIntExtra("classement", 0);
        text += classement;
        if(classement == 1)
            text += " Meilleur";
        else
            text += "eme Meilleur";
        text += " score avec vos ";
        text += getIntent().getIntExtra("score", 0);
        text += " points!";
        tv.setText(text);
    }

    public void quitter(View v){
        Intent intent = getIntent();
        intent.putExtra("data", ed.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

}